DROP TABLE users;

CREATE TABLE users
(
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  username varchar(65),
  first_name varchar(65),
  last_name varchar(65)
);

INSERT INTO users (username,first_name,last_name) values ('luis_g',"Luis Fernaando", "Garcia")