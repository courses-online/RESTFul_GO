package main

import (
  "bytes"
  "fmt"
  "io/ioutil"
  "net/http"
)

func main() {
  choose := 3

  if choose==1 {
    EmulateOperationCreate()
    
  }
  if choose==2 {

    EmulateOperationUpdate()
    
  }
  if choose==3 {

    EmulateOperationDelete()
    
  }
  
  
}

func EmulateOperationDelete() {
    url := "http://localhost:8000/users/delete/1"
  fmt.Println("URL:>", url)

  var jsonStr = []byte(`{"username": "lufgarciaqu","first_name": "Junior", "last_name": "Garcia"}`)
  req, err := http.NewRequest("DELETE", url, bytes.NewBuffer(jsonStr))
  req.Header.Set("X-Custom-Header", "myvalue")
  req.Header.Set("Content-Type", "application/json")

  client := &http.Client{}
  resp, err := client.Do(req)
  if err != nil {
    fmt.Println("Error")
    panic(err)
  }
  defer resp.Body.Close()

  fmt.Println("response Status:", resp.Status)
  fmt.Println("response Headers:", resp.Header)
  body, _ := ioutil.ReadAll(resp.Body)
  fmt.Println("response Body:", string(body))
  
}

func EmulateOperationCreate() {
  url := "http://localhost:8000/users/new"
  fmt.Println("URL:>", url)

  var jsonStr = []byte(`{"username": "lufgarciaqu","first_name": "Junior", "last_name": "Garcia"}`)
  req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
  req.Header.Set("X-Custom-Header", "myvalue")
  req.Header.Set("Content-Type", "application/json")

  client := &http.Client{}
  resp, err := client.Do(req)
  if err != nil {
    fmt.Println("Error")
    panic(err)
  }
  defer resp.Body.Close()

  fmt.Println("response Status:", resp.Status)
  fmt.Println("response Headers:", resp.Header)
  body, _ := ioutil.ReadAll(resp.Body)
  fmt.Println("response Body:", string(body))
  
}


func EmulateOperationUpdate(){

  url := "http://localhost:8000/users/update/1"
  fmt.Println("URL:>", url)

  var jsonStr = []byte(`{"username": "das","first_name": "Junior", "last_name": "Garcia"}`)
  req, err := http.NewRequest("PATCH", url, bytes.NewBuffer(jsonStr))
  req.Header.Set("X-Custom-Header", "myvalue")
  req.Header.Set("Content-Type", "application/json")

  client := &http.Client{}
  resp, err := client.Do(req)
  if err != nil {
    fmt.Println("Error")
    panic(err)
  }
  defer resp.Body.Close()

  fmt.Println("response Status:", resp.Status)
  fmt.Println("response Headers:", resp.Header)
  body, _ := ioutil.ReadAll(resp.Body)
  fmt.Println("response Body:", string(body))



}
