package main


import (
  "fmt"

  "log"
  "net/http"
  "encoding/json"

  "github.com/gorilla/mux"
  "./conect"
  "./structures"
)


func main() {
 
  conect.InitializeDataBase()
  defer conect.CloseDataBase()

  r := mux.NewRouter()

  r.HandleFunc("/users/{id}",GetUser).Methods("GET")
  r.HandleFunc("/users/new",NewUser).Methods("POST")
  r.HandleFunc("/users/update/{id}",UpdtateUser).Methods("PATCH")
  r.HandleFunc("/users/delete/{id}",DeleteUser).Methods("DELETE")




  fmt.Println("EL servidor se encuentra en el puerto 8000")
  http.ListenAndServe(":8000",r)

}

func GetUser(w http.ResponseWriter, r *http.Request) {
  vars := mux.Vars(r)

  user_id := vars["id"]

  status := "Success"
  var message string
  user := conect.GetUser(user_id)

  if user.Id <= 0 {

    status="error"
    message="user no found"
  }
  response:= structures.Response{status,user,message}

  json.NewEncoder(w).Encode(response)
}





func GetUserRequest( r *http.Request) structures.User {

  var user structures.User
  decoder := json.NewDecoder(r.Body)
  err := decoder.Decode(&user)
 
  if err != nil {
    log.Fatal(err)

    //return nil  
  }
  
  return user
  
}

func  NewUser(w http.ResponseWriter, r *http.Request) {
  user := GetUserRequest(r)
  //Nota: Se recomienda validar los datos que se van insertar en nuestra database
  //Hacer una especie de sanitize of data
  response := structures.Response{"success", conect.CreateUser(user),""}
  json.NewEncoder(w).Encode(response)
  

}

func UpdtateUser(w http.ResponseWriter, r *http.Request) {
  vars := mux.Vars(r)

  user_id := vars["id"]

  user := GetUserRequest(r)


  response := structures.Response{"success",conect.UpdtateUser(user_id,user),""}
  json.NewEncoder(w).Encode(response)
  
  
}
func DeleteUser(w http.ResponseWriter, r *http.Request) {
  vars := mux.Vars(r)
  user_id := vars["id"]
  conect.DeleteUser(user_id);

  user := structures.User{}

  response := structures.Response{"success",user,""}
  json.NewEncoder(w).Encode(response)
  
}